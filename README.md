# Software Studio 2021 Spring

## Assignment 01 Web Canvas

### Scoring

| **Basic components** | **Score** | **Check** |
| :------------------- | :-------: | :-------: |
| Basic control tools  |    30%    |     Y     |
| Text input           |    10%    |     Y     |
| Cursor icon          |    10%    |     Y     |
| Refresh button       |    10%    |     Y     |

| **Advanced tools**     | **Score** | **Check** |
| :--------------------- | :-------: | :-------: |
| Different brush shapes |    15%    |     Y     |
| Un/Re-do button        |    10%    |     Y     |
| Image tool             |    5%     |     Y     |
| Download               |    5%     |     Y     |

| **Other useful widgets** | **Score** | **Check** |
| :----------------------- | :-------: | :-------: |
| Name of widgets          |   1~5%    |     N     |

---

### How to use

1.  先點擊最右方的調色盤選擇色系，再點擊左方漸層圖中想要的顏色。
2.  選擇工具列中想要使用的功能，功能由上到下、左至右依序為：畫筆、線條粗細（圖型線條粗細也是用此調整）、橡皮擦、橡皮擦粗細、打字、重整、繪製元型、繪製三角形、繪製正方形、上一步、下一步、上傳檔案、下載圖片。
3.  使用打字時，請先選擇字型和字體大小漢字的顏色，再點擊畫布，就會從點擊處開始出現文字。

### Function description

    1. 調色盤我是使用了另外兩個canvas畫布去實作，皆是使用 createLinearGradient() 這個function來製造出一個色系圖和一個漸層圖，再用getImageData()取得色系圖點擊處的rgbColor將其顯現成漸層圖，同理也能取得點擊漸層圖的rgbColor轉入至繪製時的顏色上。
    2. 我是藉由點擊按鈕後，將其轉成不同的mode來實作的，所以會看到CurrentMode這個變數和相關的轉換Mode的function，再根據不同Mode來判斷監聽到mousedown、mousemove分別要執行什麼。
    3. text輸入我是將lab04的概念重現，直接將文字畫在畫布上，所以要預先設定好字體大小、顏色、字型，而BaskSpace則是利用undo的概念去做。
    4. undo、redo是用兩個stack來實作，繪製時都先存入其中一個，做undo的時候再將pop出來的存入另一個，並維護。

### Gitlab page link

    "https://108062126.gitlab.io/AS_01_WebCanvas"

### Others (Optional)

    辛苦了！

<style>
table th{
    width: 100%;
}
</style>
