// main canvas
var canvas = document.getElementById("cvs");
var ctx = canvas.getContext("2d");
var width = canvas.width;
var height = canvas.height;

var CurrentMode = "pencil";

let isDrawing = false;
let [lastX, lastY] = [0, 0];
var n = 0;
var triX = 0, triY = 0;
var textX = 0, textY = 0;
var imgStack = [];
var RStack = [];
var flag = false;
var textString = "";

canvas.addEventListener('mousedown', (e) => {
    isDrawing = true;
    [lastX, lastY] = [e.offsetX, e.offsetY];
    var ImgData = ctx.getImageData(0, 0, width, height);
    imgStack.push(ImgData);
    RStack = [];

    if(CurrentMode === "text"){
        var size = document.getElementById("textsize").value;
        var fonttype = document.getElementById("textfont").value;
        ctx.fillStyle = rgbColor;
        ctx.font = size + "px " + fonttype;
        textX = lastX;
        textY = lastY;
        n = 0;
        textString = "";
    }
    else if(CurrentMode === "triangle"){
        triX = lastX;
        triY = lastY;
    }
    else if(CurrentMode === "square"){
        triX = lastX;
        triY = lastY;
    }
    else if(CurrentMode === "circle"){
        triX = lastX;
        triY = lastY;
    }
});
canvas.addEventListener('mousemove', (e) => {
    let strokeWidth = document.getElementById("Stroke").value;
    ctx.lineCap = 'round';
    ctx.lineJoin = 'round';
    if(isDrawing === false)
        return;
    if(CurrentMode === "pencil"){
    
        console.log(strokeWidth);
        ctx.strokeStyle = rgbColor;
        ctx.lineWidth = strokeWidth;
        ctx.beginPath();
        ctx.moveTo(lastX, lastY);
        ctx.lineTo(e.offsetX, e.offsetY);
        [lastX, lastY] = [e.offsetX, e.offsetY];
        ctx.stroke();
    }
    else if(CurrentMode === "eraser"){
        let eraserWidth = document.getElementById("eraserWidth").value;
        ctx.clearRect((lastX - eraserWidth/2), (lastY - eraserWidth/2), eraserWidth, eraserWidth);
        [lastX, lastY] = [e.offsetX, e.offsetY];
    }
    else if(CurrentMode === "circle"){
        ctx.strokeStyle = rgbColor;
        ctx.lineWidth = strokeWidth;
        var radius = Math.sqrt(Math.pow((triY - e.offsetY), 2) + Math.pow((triX - e.offsetX), 2))/2;
        ctx.beginPath();
        var ImgData = imgStack.pop();
        ctx.putImageData(ImgData, 0, 0);
        imgStack.push(ImgData);
        ctx.arc((triX + e.offsetX)/2, (triY + e.offsetY)/2, radius, 0, 2 * Math.PI, true);
        ctx.stroke();
    }
    else if(CurrentMode === "triangle"){
        ctx.strokeStyle = rgbColor;
        ctx.lineWidth = strokeWidth;
        ctx.beginPath();
        ctx.moveTo(triX, triY);
        ctx.lineTo(e.offsetX, e.offsetY);

        if(e.offsetX > triX){
            ctx.lineTo(e.offsetX - 2 * (e.offsetX - triX), e.offsetY);
        }
        else{
            ctx.lineTo(e.offsetX + 2 * (triX - e.offsetX), e.offsetY);
        }
        ctx.lineTo(triX, triY);
        var ImgData = imgStack.pop();
        ctx.putImageData(ImgData, 0, 0);
        imgStack.push(ImgData);
        ctx.stroke();
        ctx.closePath();
    }
    else if(CurrentMode === "square"){
        ctx.strokeStyle = rgbColor;
        ctx.lineWidth = strokeWidth;
        ctx.beginPath();
        ctx.moveTo(triX, triY);
        ctx.lineTo(triX, e.offsetY);
        ctx.lineTo(e.offsetX, e.offsetY);
        ctx.lineTo(e.offsetX, triY);
        ctx.lineTo(triX, triY);
        var ImgData = imgStack.pop();
        ctx.putImageData(ImgData, 0, 0);
        imgStack.push(ImgData);
        ctx.stroke();
        ctx.closePath();
    }
});
canvas.addEventListener('mouseup',() => {
    isDrawing = false;
});
canvas.addEventListener('mouseout',() => isDrawing = false);

function undo(){
    if(imgStack.length > 0){
        if(RStack.length === 0){
            var temp = ctx.getImageData(0, 0, width, height);
            RStack.push(temp);
        }
        var ImgData = imgStack.pop();
        ctx.putImageData(ImgData, 0, 0);
        RStack.push(ImgData);
    }
}

function redo(){
    var ImgData = RStack.pop();
    ctx.putImageData(ImgData, 0, 0);
    imgStack.push(ImgData);
}

function ChangeModeToPencil(){
    CurrentMode = "pencil";
    document.getElementById("cvs").style.cursor = "url('pencil.cur'), auto";
    console.log("pp");
}

function ChangeModeToEraser(){
    CurrentMode = "eraser";
    document.getElementById("cvs").style.cursor = "url('Eraser.cur'), auto";
    console.log("ee");
}

function ChangeModeToText(){
    CurrentMode = "text";
    document.getElementById("cvs").style.cursor = "text";
    console.log("tt");
}

function ChangeModeToCircle(){
    CurrentMode = "circle";
    document.getElementById("cvs").style.cursor = "url('circle.cur'), auto";
    console.log("cc");
}

function ChangeModeToTriangle(){
    CurrentMode = "triangle";
    document.getElementById("cvs").style.cursor = "url('Triangle.cur'), auto";
    console.log("ttt");
}

function ChangeModeToSquare(){
    CurrentMode = "square";
    document.getElementById("cvs").style.cursor = "url('squ.cur'), auto";
    console.log("ss");
}

// color selector

var colorBlock = document.getElementById("color-block");
var CBctx = colorBlock.getContext('2d');
var CBwidth = colorBlock.width;
var CBheight = colorBlock.height;

var colorStrip = document.getElementById("color-strip");
var CSctx = colorStrip.getContext('2d');
var CSwidth = colorStrip.width;
var CSheight = colorStrip.height;

var colorLabel = document.getElementById("color-label");

var colorX = 0;
var colorY = 0;
var drag = false;
var rgbColor = "rgba(255, 0, 0, 1)";

CBctx.rect(0, 0, CBwidth, CBheight);
fillGradient();
CSctx.rect(0, 0, CSwidth, CSheight);
var CSgrd = CSctx.createLinearGradient(0, 0, 0, CSheight);

CSgrd.addColorStop(0, "rgba(255, 0, 0, 1)" );
CSgrd.addColorStop(0.17, "rgba(255, 255, 0, 1)" );
CSgrd.addColorStop(0.34, "rgba(0, 255, 0, 1)" );
CSgrd.addColorStop(0.51, "rgba(0, 255, 255, 1)" );
CSgrd.addColorStop(0.68, "rgba(0, 0, 255, 1)" );
CSgrd.addColorStop(0.85, "rgba(255, 0, 255, 1)" );
CSgrd.addColorStop(1, "rgba(255, 0, 0, 1)" );
CSctx.fillStyle = CSgrd;
CSctx.fill();

colorStrip.addEventListener("click", (e) => {
    colorX = e.offsetX;
    colorY = e.offsetY;

    var ImageData = CSctx.getImageData(colorX, colorY, 1, 1).data;
    rgbColor = "rgba(" + ImageData[0] + ',' + ImageData[1] + ',' + ImageData[2] + ",1)";
    fillGradient();
}, false);

colorBlock.addEventListener("mousedown", (e) => {
    drag = true;

    colorX = e.offsetX;
    colorY = e.offsetY;

    var ImageData = CBctx.getImageData(colorX, colorY, 1, 1).data;
    rgbColor = "rgba(" + ImageData[0] + ',' + ImageData[1] + ',' + ImageData[2] + ",1)";
    colorLabel.style.background = rgbColor;

}, false);

colorBlock.addEventListener("mouseup", () => {
    drag = false;
}, false);

colorBlock.addEventListener("mouseout", () => {
    drag = false;
}, false);

colorBlock.addEventListener("mousemove", (e) => {
    if(drag){
        colorX = e.offsetX;
        colorY = e.offsetY;

        var ImageData = CBctx.getImageData(colorX, colorY, 1, 1).data;
        rgbColor = "rgba(" + ImageData[0] + ',' + ImageData[1] + ',' + ImageData[2] + ",1)";
        colorLabel.style.background = rgbColor;
    }
}, false);

function fillGradient(){
    CBctx.fillStyle = rgbColor;
    CBctx.fillRect(0, 0, CBwidth, CBheight);

    var grdWhite = CSctx.createLinearGradient(0, 0, CBwidth, 0);
    grdWhite.addColorStop(0, 'rgba(255,255,255,1)');
    grdWhite.addColorStop(1, 'rgba(255,255,255,0)');
    CBctx.fillStyle = grdWhite;
    CBctx.fillRect(0, 0, CBwidth, CBheight);

    var grdBlack = CSctx.createLinearGradient(0, 0, 0, CBheight);
    grdBlack.addColorStop(0, 'rgba(0, 0, 0, 0)');
    grdBlack.addColorStop(1, 'rgba(0, 0, 0, 1)');
    CBctx.fillStyle = grdBlack;
    CBctx.fillRect(0, 0, CBwidth, CBheight);
}

//Reset
function Reset(){
    alert("確定要清空畫布嗎？");
    ctx.clearRect(0, 0, width, height);
    imgStack = [];
    RStack = [];
    CurrentMode = "pencil";
    isDrawing = false;
    [lastX, lastY] = [0, 0];
    n = 0;
    triX = 0, triY = 0;
    textX = 0, textY = 0;
    flag = false;
    textString = "";
    document.getElementById("cvs").style.cursor = "url('pencil.cur'), auto";
}

//upload & download
function saveFile(){
    var link = document.getElementById("download");
    link.download = "image.jpg";
    link.href = canvas.toDataURL("image/jpeg");
    link.click();
}

function loadFile(e){
    var file = e.files[0];
    var src = URL.createObjectURL(file);
    var img = new Image();
    img.src = src;
    img.onload = function(){
        ctx.drawImage(this, 0, 0, width, height);
    }
}

function keyboard(event){
    var x = event.keyCode;
    var tempt = String.fromCharCode(x);
    textString = textString + tempt;
    if(CurrentMode === "text"){
        console.log(tempt);
            ctx.fillText(textString, textX, textY);
    }
}

function Back(event){
    var x = event.keyCode;
    if(x === 8 && CurrentMode === "text"){
        console.log(x);
        var NtextString = textString.substring(0, textString.length - 1);
        var ImgData = imgStack.pop();
        ctx.putImageData(ImgData, 0, 0);
        imgStack.push(ImgData);
        ctx.fillText(NtextString, textX, textY);
        textString = NtextString;
    }
}